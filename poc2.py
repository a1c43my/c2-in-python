import socket,time,threading

def killC(c):
    print('closing '+str(c))
    c.close()


def handler(c,a):
    print('new thread!!!!!!!!!!!!!!!!!!!!')
    while True:
        try:
            newconn = c.recv(2048).decode()
            print(newconn)
            #c.send("hello".encode())
            if newconn == 'goodbye':
                print(newconn)
                c.send("goodbye".encode())
                time.sleep(1)
            elif newconn == 'ready':
                print('wake command')
                c.send("awake".encode())
            else:
                print('waiting for cmd')
                msg = input('enter cmd : ')
                c.send(msg.encode())
            time.sleep(1)
        except KeyboardInterrupt:
            print('you killed inside of loop')
        except Exception:
            print('something killed inside of loop')
            time.sleep(2)

def initServ():
    while True:
        conn = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        conn.bind(("127.0.0.1",9000))
        print('binded')
        conn.listen(5)
        client,addr = conn.accept()
        print('waiting')
        t = threading.Thread(name="Client: ",target=handler,args=((client,addr)))
        t.start()

try:
    initServ()
except KeyboardInterrupt:
    print('you stopped this')
except Exception:
    print('nother error')